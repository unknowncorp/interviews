FROM ubuntu 

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get upgrade -yq && \
  apt-get -yq install sqlite python ruby openjdk-8-jdk golang php nodejs npm vim emacs nano curl wget

RUN echo "deb https://dl.bintray.com/sbt/debian /" >> /etc/apt/sources.list.d/sbt.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
RUN apt-get update && apt-get -yq install sbt

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR /root 
COPY resources/Chinook_Sqlite.sql Chinook_Sqlite.sql 
COPY resources/auth_decorator.py auth_decorator.py 
COPY resources/coinbase_api.txt coinbase_api.txt

RUN sqlite3 interview.db < Chinook_Sqlite.sql
