#!/bin/bash 

if [ ! -d ./scratch ]; then
	mkdir ./scratch
fi

cd ./scratch
SCRATCH_DIR=`pwd`
cd ../

docker run -it --rm --mount type=bind,source=$SCRATCH_DIR,target=/root/scratch interview /bin/bash
