#!/bin/bash

echo "########################################################################################"
echo "#                                                                                      #"
echo "# Assuming you have Docker running, and enough space for what's about to happen.       #"
echo "#                                                                                      #"
echo "########################################################################################"

git pull
docker build -t interview ./

echo "########################################################################################"
echo "#                                                                                      #"
echo "# Done!  now run ./connect.sh to get started. The scratch directory is available in    #"
echo "# both the host and container contexts. Be sure to clean it out when you're done.      #"
echo "#                                                                                      #"
echo "########################################################################################"
