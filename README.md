# Interviews
Resources for tech interviews

## General Use
Unsure what to do?  Run ./setup.sh.  It may help.  

## DB
In /root there's a sqlite3 db called interview.db that's the Chinook data set.  Ask smart questions.

## Host <-> Container connection
The __scratch__ folder is bind mounted in the container.  Anything you put there will be available in both contexts.  This allows for scripts to be written in a visual editor of choice on the host machine, and run within the interview context of the container.