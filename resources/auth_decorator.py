
def auth_decoration(original_func, test_func, login_url=settings.LOGIN_URL, 403_url=settings.403_URL):

  def checklogin(request):
    if test_func(request):
      return original_func(request)

    elif request.user.is_authenticated():
      register_flash_message(request, 'Sorry, you do not have permission to access that page.')
      return HttpResponseRedirect('%s?from_page=%s' % (403_url, quote(request.get_full_path())))

    return HttpResponseRedirect('%s?from_page=%s' % (login_url, quote(request.get_full_path())))

  return checklogin

#
#request object is defined somewhere in here
#draw_admin_view is defined somewhere in here
#

draw_admin_view = auth_decoration(draw_admin_view, lambda r: r.user.is_authenticated() and r.user.is_editor())

#Other Stuff

